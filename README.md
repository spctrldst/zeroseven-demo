# Zeroseven Demo

An assigned project to transform a PhotoShop Design file into a working,
responsive website.

# Requirements

1. w3c compliance
2. IE9+ compatibility
3. HTML5 compatibility
4. Optimised web assets
5. SEO and searchable elements
6. Appropriate use of Bootstrap and other libraries
7. Responsive with a collapsible menu and set mobile breakpoint

# Notes

- I would write this in preprocessed languages but pressed for time so just going to write raw production code for now.
- Would normally also host it as a nodejs app but static will have to do.
- The gitlab hosted site is available at the following link: https://spctrldst.gitlab.io/zeroseven-demo/home.html
